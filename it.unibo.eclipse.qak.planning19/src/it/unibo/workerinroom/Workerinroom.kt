/* Generated by AN DISI Unibo */ 
package it.unibo.workerinroom

import it.unibo.kactor.*
import alice.tuprolog.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
	
class Workerinroom ( name: String, scope: CoroutineScope ) : ActorBasicFsm( name, scope){
 	
	override fun getInitialState() : String{
		return "s0"
	}
		
	override fun getBody() : (ActorBasicFsm.() -> Unit){
		
		var mapEmpty    = false
		val mapname     ="roomMapWithTable"
		
		var Curmove     = "" 
		var curmoveIsForward = false 
		
		//REAL ROBOT
		//var StepTime   = 1000 	 
		//var PauseTime  = 500 
		
		//VIRTUAL ROBOT
		var StepTime   = 330	//for virtual
		var PauseTime  = 500
		
		var PauseTimeL  = PauseTime.toLong()
		return { //this:ActionBasciFsm
				state("s0") { //this:State
					action { //it:State
						solve("consult('moves.pl')","") //set resVar	
						itunibo.planner.plannerUtil.initAI(  )
						itunibo.planner.moveUtils.loadRoomMap(myself ,mapname )
						itunibo.planner.moveUtils.showCurrentRobotState(  )
							val MapStr =  itunibo.planner.plannerUtil.getMapOneLine()  
						forward("modelUpdate", "modelUpdate(roomMap,$MapStr)" ,"resourcemodel" ) 
					}
					 transition( edgeName="goto",targetState="tableEast", cond=doswitch() )
				}	 
				state("tableEast") { //this:State
					action { //it:State
						itunibo.planner.plannerUtil.setGoal( 5, 3  )
						itunibo.planner.moveUtils.doPlan(myself)
					}
					 transition( edgeName="goto",targetState="executePlannedActions", cond=doswitch() )
				}	 
				state("executePlannedActions") { //this:State
					action { //it:State
						solve("retract(move(M))","") //set resVar	
						if(currentSolution.isSuccess()) { Curmove = getCurSol("M").toString() 
						              curmoveIsForward=(Curmove == "w")
						 }
						else
						{ Curmove = ""; curmoveIsForward=false
						 }
						println("executePlannedActions doing $Curmove")
					}
					 transition( edgeName="goto",targetState="checkAndDoAction", cond=doswitchGuarded({(Curmove.length>0) }) )
					transition( edgeName="goto",targetState="goalOk", cond=doswitchGuarded({! (Curmove.length>0) }) )
				}	 
				state("goalOk") { //this:State
					action { //it:State
						itunibo.planner.moveUtils.showCurrentRobotState(  )
							val MapStr =  itunibo.planner.plannerUtil.getMapOneLine()  
						forward("modelUpdate", "modelUpdate(roomMap,$MapStr)" ,"resourcemodel" ) 
					}
				}	 
				state("checkAndDoAction") { //this:State
					action { //it:State
					}
					 transition( edgeName="goto",targetState="doForwardMove", cond=doswitchGuarded({curmoveIsForward}) )
					transition( edgeName="goto",targetState="doTheMove", cond=doswitchGuarded({! curmoveIsForward}) )
				}	 
				state("doTheMove") { //this:State
					action { //it:State
						itunibo.planner.moveUtils.rotate(myself ,Curmove, PauseTime )
					}
					 transition( edgeName="goto",targetState="executePlannedActions", cond=doswitch() )
				}	 
				state("doForwardMove") { //this:State
					action { //it:State
						delay(PauseTimeL)
						itunibo.planner.moveUtils.attemptTomoveAhead(myself ,StepTime )
					}
					 transition(edgeName="t00",targetState="handleStepOk",cond=whenDispatch("stepOk"))
					transition(edgeName="t01",targetState="hadleStepFail",cond=whenDispatch("stepFail"))
				}	 
				state("handleStepOk") { //this:State
					action { //it:State
						itunibo.planner.moveUtils.updateMapAfterAheadOk(myself)
					}
					 transition( edgeName="goto",targetState="executePlannedActions", cond=doswitch() )
				}	 
				state("hadleStepFail") { //this:State
					action { //it:State
						println("NEVER HERE!!!")
					}
				}	 
			}
		}
}
