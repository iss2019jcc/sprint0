/* Generated by AN DISI Unibo */ 
package it.unibo.commandsender

import it.unibo.kactor.*
import alice.tuprolog.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
	
class Commandsender ( name: String, scope: CoroutineScope ) : ActorBasicFsm( name, scope){
 	
	override fun getInitialState() : String{
		return "s0"
	}
		
	override fun getBody() : (ActorBasicFsm.() -> Unit){
		return { //this:ActionBasciFsm
				state("s0") { //this:State
					action { //it:State
						forward("setGoal", "setGoal(6,3)" ,"workerinroom" ) 
						stateTimer = TimerActor("timer_s0", 
							scope, context!!, "local_tout_commandsender_s0", 3000.toLong() )
					}
					 transition(edgeName="t00",targetState="suspend",cond=whenTimeout("local_tout_commandsender_s0"))   
				}	 
				state("suspend") { //this:State
					action { //it:State
						forward("suspend", "suspend" ,"workerinroom" ) 
						stateTimer = TimerActor("timer_suspend", 
							scope, context!!, "local_tout_commandsender_suspend", 10000.toLong() )
					}
					 transition(edgeName="t11",targetState="resume",cond=whenTimeout("local_tout_commandsender_suspend"))   
				}	 
				state("resume") { //this:State
					action { //it:State
						forward("resume", "resume" ,"workerinroom" ) 
					}
					 transition(edgeName="t22",targetState="backHome",cond=whenEvent("goalReached"))
				}	 
				state("backHome") { //this:State
					action { //it:State
						forward("setGoal", "setGoal(0,0)" ,"workerinroom" ) 
						stateTimer = TimerActor("timer_backHome", 
							scope, context!!, "local_tout_commandsender_backHome", 3000.toLong() )
					}
					 transition(edgeName="t33",targetState="suspendBackHome",cond=whenTimeout("local_tout_commandsender_backHome"))   
				}	 
				state("suspendBackHome") { //this:State
					action { //it:State
						forward("suspend", "suspend" ,"workerinroom" ) 
						stateTimer = TimerActor("timer_suspendBackHome", 
							scope, context!!, "local_tout_commandsender_suspendBackHome", 10000.toLong() )
					}
					 transition(edgeName="t14",targetState="resumeBackHome",cond=whenTimeout("local_tout_commandsender_suspendBackHome"))   
				}	 
				state("resumeBackHome") { //this:State
					action { //it:State
						forward("resume", "resume" ,"workerinroom" ) 
					}
					 transition(edgeName="t25",targetState="backHome",cond=whenEvent("goalReached"))
				}	 
			}
		}
}
